package fonts;

import fonts.service.FontService;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;
import java.util.Objects;

public class LinuxFontsTest {

    private FontService fontService = new FontService();

    @Test
    public void linuxFontsTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource("fonts.conf")).getFile());
        List<String> paths = fontService.getLinuxFontsFolders(file.getAbsolutePath());
        Assert.assertEquals(4, paths.size());
    }

}
