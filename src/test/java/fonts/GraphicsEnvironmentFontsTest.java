package fonts;

import fonts.service.FontService;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

public class GraphicsEnvironmentFontsTest {

    private FontService fontService = new FontService();

    @Test
    public void test() {
        Set<String> awtFonts = fontService.findAwtFonts();
        Assert.assertNotEquals(0, awtFonts.size());
    }

}
