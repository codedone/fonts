package fonts.controller;

import fonts.controller.dto.FontsDto;
import fonts.domain.FontInfo;
import fonts.service.FontService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FontController {
    
    @Autowired
    private FontService fontService;

    @GetMapping("api/fonts")
    public List<FontsDto> fonts() throws Exception {
        return fontService.getFontInfoSet()
                    .stream()
                    .map(FontInfo::getName)
                    .map(fileName -> new FontsDto(fileName, "api/font/" + fileName))
                    .collect(Collectors.toList());
    }


    @GetMapping("api/font/{fontName}")
    public byte[] getFont(@PathVariable String fontName) throws Exception {
        return fontService.getFont(fontName);
    }
}