package fonts.controller.dto;

import java.util.List;

public class FontsTransferDto {

    private List<FontsDto> fonts;

    public FontsTransferDto(List<FontsDto> fonts) {
        this.fonts = fonts;
    }

    public FontsTransferDto() {
    }

    public List<FontsDto> getFonts() {
        return fonts;
    }

    public void setFonts(List<FontsDto> fonts) {
        this.fonts = fonts;
    }

    @Override
    public String toString() {
        return "FontsTransferDto{" +
                "fonts=" + fonts +
                '}';
    }
}
