package fonts.controller.dto;

import java.util.Objects;

public class FontsDto {

    private String name;
    private String path;

    public FontsDto(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public FontsDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FontsDto fontsDto = (FontsDto) o;
        return Objects.equals(name, fontsDto.name) &&
                Objects.equals(path, fontsDto.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, path);
    }

    @Override
    public String toString() {
        return "FontsDto{" +
                "name='" + name + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
