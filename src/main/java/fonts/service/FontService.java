package fonts.service;

import fonts.domain.FontInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FontService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private static String OS = System.getProperty("os.name").toLowerCase();

    private volatile Set<FontInfo> fontInfoSet = new HashSet<>();

    @PostConstruct
    public void init() throws Exception {
       log.info("Operation system " + OS);

       Set<String> fontsPath = findFonts();

       fontInfoSet = fontsPath.stream()
                .map(it -> new FontInfo(getNameByPath(it), it))
                .collect(Collectors.toSet());

        fontInfoSet.forEach(fontInfo -> log.info(fontInfo.getPath()));
    }

    private String getNameByPath(String path) {
        File file = new File(path);
        return file.getName();
    }

    public Set<FontInfo> getFontInfoSet() {
        return fontInfoSet;
    }

    public Set<String> findFonts() throws Exception {
        Set<String> set = findSystemFonts();
        set.addAll(findAwtFonts());
        return set;
    }

    public Set<String> findSystemFonts() throws Exception {
        return getFontsPaths().stream()
                .map(folder -> Paths.get(folder))
                .flatMap(this::walk)
                .filter(Objects::nonNull)
                .filter(Files::isRegularFile)
                .map(Path::toString)
                .sorted()
                .collect(Collectors.toSet());
    }

    public Set<String> findAwtFonts() {
        return Stream.of(GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts())
                .map(this::getPath)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    private String getPath(Font font) {
        Object font2D = null;
        try {
            font2D = Class.forName("sun.font.FontUtilities").getDeclaredMethod("getFont2D", new Class[]{Font.class}).invoke(null, font);
        } catch (Throwable ignored) {
            try {
                font2D = Class.forName("sun.font.FontManager").getDeclaredMethod("getFont2D", new Class[]{Font.class}).invoke(null, font);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }

        if (font2D != null) {
            try {
                Field platNameField = Class.forName("sun.font.PhysicalFont").getDeclaredField("platName");
                platNameField.setAccessible(true);
                return (String) platNameField.get(font2D);
            } catch (Exception e) {
                log.info(e.getMessage());
            }
        }

        return null;
    }

    private Stream<? extends Path> walk(Path path) {
        try {
            return Files.walk(path);
        } catch (IOException e) {
           log.info(e.getMessage());
           return null;
        }
    }

    public byte[] getFont(String fontName) throws Exception {
        String fileName = fontInfoSet.stream()
                .filter(it -> it.getName().equals(fontName))
                .map(FontInfo::getPath)
                .findFirst()
                .get();
        return readFont(fileName);
    }

    public byte[] readFont(String fileName) throws Exception {
        return Files.readAllBytes(Paths.get(fileName));
    }

    private List<String> getFontsPaths() throws Exception {
        //Widnows OS
        if ((OS.contains("win"))) return Collections.singletonList(System.getenv("windir") + File.separator + "FONTS" + File.separator);

        // Fonts in Mac OS X always in folder /System/Library/Fonts/
        if ((OS.contains("mac"))) {
            String user = System.getenv().get("USER");
            List<String> paths = new ArrayList<>();
            paths.add("/Users/" + user + "/Library/Fonts");
            paths.add("/System/Library/Fonts/");
            return paths;
        }

        //Linux OS
        return getLinuxFontsFolders("/etc/fonts/fonts.conf");
    }

    public List<String> getLinuxFontsFolders(String fileName) throws Exception {
        List<String> result = new ArrayList<>();
        File fXmlFile = new File(fileName);
        FileInputStream fileIS = new FileInputStream(fXmlFile);
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setValidating(false);
        builderFactory.setNamespaceAware(true);
        builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
        builderFactory.setFeature("http://xml.org/sax/features/validation", false);
        builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
        builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document xmlDocument = builder.parse(fileIS);
        XPath xPath = XPathFactory.newInstance().newXPath();
        String expression = "/fontconfig/dir";
        NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
        for (int n = 0; n < nodeList.getLength(); n++) {
            Node node = nodeList.item(n);
            result.add(node.getTextContent());
        }
        return result
                .stream()
                .filter(folder -> folder.startsWith("/"))
                .collect(Collectors.toList());
    }
}
