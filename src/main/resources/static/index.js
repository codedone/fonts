window.onload = function () {
    const xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            const fonts = JSON.parse(this.responseText);

            console.log("Loaded fonts " + fonts);

            fontsInit(fonts);
            doFont();

            new FroalaEditor('#editor', {
                quickInsertEnabled: false,
                fontSizeUnit: 'px',
                toolbarButtons: ['fontFamily', 'fontSize', 'bold', 'italic', 'underline', 'strikeThrough'],
                fontFamilySelection: true
            });

            const frm = frames['editor'].document;
            const fontsFrame = document.getElementsByClassName("fonts-frame");
            const otherhead = frm.getElementsByTagName("head")[0];
            while (fontsFrame.length > 0) {
                otherhead.appendChild(fontsFrame[0]);
            }

            document.addEventListener('DOMContentLoaded', function () {
                const fonts = document.getElementsByClassName('fonts');
                for (let i = 0; i < fonts.length; i++) {
                    const fontName = fonts[i].getElementsByClassName('fontName')[0];
                    const fontPath = fonts[i].getElementsByClassName('fontPath')[0];
                    const splitFontName = fontName.innerText.split(".");
                    const styleContainer = document.getElementById('style');
                    const stylesInnerHtml = styleContainer.innerHTML += '<style> \n @font-face { \n font-family:' + fontName.innerText + ';\n' + 'src: url(/' + fontPath.innerText + ');' + ' \n } \n</style>';
                    let y = (editor.contentWindow || editor.contentDocument);
                    if (y.document) y = y.document;
                    y.body.innerHTML = stylesInnerHtml;
                }
            });
        }
    };

    xhttp.open("GET", "api/fonts", true);
    xhttp.send();
};


function fontsInit(fonts) {
    const editor = document.getElementById("editor");
    editor.contentWindow.document.designMode = "On";

    const select = document.getElementById("mySelect");
    const style = document.createElement("style");

    const content = editor.contentWindow || editor.contentDocument;
    const head = content.document.head;

    if (Array.isArray(fonts) && !fonts.length) {
        return;
    }

    for (let index = 0; index < fonts.length; index++) {
        const item = fonts[index];
        const option = document.createElement("option");
        const fontFace = document.createTextNode("@font-face{font-family: '" + item.name + "';src: url('" + item.path + "');}");

        option.text = item.name;

        select.appendChild(option);
        style.appendChild(fontFace);
    }

    head.appendChild(style);
    fontChange(fonts[0].name);
}

function fontChange(family) {
    const editor = document.getElementById("editor");
    const content = editor.contentWindow || editor.contentDocument;
    const body = content.document.body;

    body.style.fontFamily = "'" + family + "', sans-serif";
}

function doStyle(style) {
    const editor = document.getElementById("editor");
    editor.contentWindow.document.execCommand(style, false, null);
}

function doFont() {
    const editor = document.getElementById("editor");
    const e = document.getElementById("ddlViewBy");
    const strUser = e.options[e.selectedIndex].value;
    editor.contentWindow.document.execCommand("fontSize", false, strUser);
}

function fontEditor(fontName) {
    const editor = document.getElementById("editor");
    editor.contentWindow.document.execCommand("fontName", false, fontName);
}